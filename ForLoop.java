
public class ForLoop {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		for(int i=0;i<10;i++){
			System.out.println("The value of i:" + i);
		}
		
		int[] arrNum={10,20,30};
		for(int i=0;i<arrNum.length;i++){
			System.out.println("Element of array: " + arrNum[i]);
		}
		
		
		//Shorcut
		for(int number : arrNum){
			System.out.println("The value is: " + number);
		}
		
		//String
		String[] names={"BMW","Lexus"};
		
		for(String name : names){
			System.out.println("Model: " + name);
		}
		

	}

}
