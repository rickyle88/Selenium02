
public class ReverseString {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		String inputStr="This     is a test string";
		String outputStr=reverse(inputStr);
		System.out.println(outputStr);

	}
	
	public static String reverse(String input){
		
		String reverseStr = "";
		
		String[] arrStr=input.trim().split("\\s+");
		
		for(String itemStr:arrStr){
			reverseStr=itemStr + " " + reverseStr;
		}
		
		return reverseStr;
	}

}
