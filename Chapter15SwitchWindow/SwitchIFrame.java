package Chapter15SwitchWindow;

import static org.junit.Assert.*;

import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.MarionetteDriver;

public class SwitchIFrame {
	
	private WebDriver driver;
	String baseURL="https://letskodeit.teachable.com/p/practice";
	
	@Before
	public void setUp() throws Exception {
		//Set
		System.setProperty("webdriver.gecko.driver", "E:\\Ralis\\Selenium\\geckodriver.exe");
			
		driver=new MarionetteDriver();		
		
		//Maximize browser's window
		driver.manage().window().maximize();	
		
		driver.manage().timeouts().implicitlyWait(3, TimeUnit.SECONDS);
}
	@Test
	public void test() throws InterruptedException {
		
		driver.get(baseURL);
		
		//Switch to frame with id frame
		//driver.switchTo().frame("courses-iframe");
		
		//Switch to frame with name frame
		//driver.switchTo().frame("iframe-name");
		
		//Switch to frame with index
		driver.switchTo().frame(0);
		
		
		WebElement searchBox=driver.findElement(By.id("search-courses"));
		searchBox.sendKeys("java");
		
		//Switch to parent window
		driver.switchTo().defaultContent();
		
		//Test
		driver.findElement(By.id("name")).sendKeys("Parent");;
	}

	@After
	public void tearDown() throws Exception {
		//driver.quit();
	}

	

}
