package Chapter15SwitchWindow;

import static org.junit.Assert.*;

import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.MarionetteDriver;

public class SwitchAlert {
	
	private WebDriver driver;
	String baseURL="https://letskodeit.teachable.com/p/practice";
	
	@Before
	public void setUp() throws Exception {
		//Set
		System.setProperty("webdriver.gecko.driver", "E:\\Ralis\\Selenium\\geckodriver.exe");
			
		driver=new MarionetteDriver();		
		
		//Maximize browser's window
		driver.manage().window().maximize();	
		
		driver.manage().timeouts().implicitlyWait(3, TimeUnit.SECONDS);
		
		driver.get(baseURL);
}
	@Test
	public void test1() throws InterruptedException {	
		driver.findElement(By.id("name")).sendKeys("Test1");
		
		Thread.sleep(2000);
		//Click Alert button (alert)
		driver.findElement(By.id("alertbtn")).click();
		Thread.sleep(2000);
		//Switch to alert
		Alert alert=driver.switchTo().alert();
		alert.accept();
	}
	
	@Test
	public void test2() throws InterruptedException {	
		
		Thread.sleep(2000);
		//Click Alert button (alert)
		driver.findElement(By.id("confirmbtn")).click();
		Thread.sleep(2000);
		//Switch to alert
		Alert alert=driver.switchTo().alert();
		alert.dismiss();
}

	@After
	public void tearDown() throws Exception {
		//driver.quit();
	}

	

}
