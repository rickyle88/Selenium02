package Chapter15SwitchWindow;

import static org.junit.Assert.*;

import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.MarionetteDriver;

public class SwitchWindow {
	
	private WebDriver driver;
	String baseURL="https://letskodeit.teachable.com/p/practice";
	
	@Before
	public void setUp() throws Exception {
		//Set
		System.setProperty("webdriver.gecko.driver", "E:\\Ralis\\Selenium\\geckodriver.exe");
			
		driver=new MarionetteDriver();		
		
		//Maximize browser's window
		driver.manage().window().maximize();	
		
		driver.manage().timeouts().implicitlyWait(3, TimeUnit.SECONDS);
}
	@Test
	public void test() throws InterruptedException {
		
		driver.get(baseURL);
		
		//Parent window
		//WebElement openNewWindow=driver.findElement(By.id("openwindow"));
		//openNewWindow.click();
		
		
		//Search box in new (child) window
		
		
		
		//1.Get the handle
		String parentHandle=driver.getWindowHandle();
		System.out.println("Parent Handle: " + parentHandle);
				
		//Find Open Window and Click to open new window
		WebElement openNewWindow=driver.findElement(By.id("openwindow"));
		openNewWindow.click();
		
		//Get all handles
		Set<String> handles=driver.getWindowHandles();
		
		//Switching between handles
		for(String handle:handles){
			System.out.println(handle);
			if(!handle.equals(parentHandle)){
				driver.switchTo().window(handle);
				
				Thread.sleep(2000);
				
				//Search box in new (child) window
				WebElement searchBox=driver.findElement(By.id("search-courses"));
				searchBox.sendKeys("java");	
				
				Thread.sleep(2000);
				
				//Close current window
				driver.close();
				
				break;
			}
		}
		
		//Switch back to the parent window
		driver.switchTo().window(parentHandle);
		
		driver.findElement(By.id("name")).sendKeys("Parent windows");
		
		
	}

	@After
	public void tearDown() throws Exception {
		//driver.quit();
	}

	

}
