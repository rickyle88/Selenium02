package Chapter07LocateElements;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.MarionetteDriver;

public class ByIdXpath {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		

		//Set
		System.setProperty("webdriver.gecko.driver", "E:\\Ralis\\Selenium\\geckodriver.exe");
		
		WebDriver driver=new MarionetteDriver();
		String baseURL="http://www.google.com";
		driver.get(baseURL);
		System.out.println("Application title is ============="+driver.getTitle());
		
		//Enter search string and then click Search button
		driver.findElement(By.id("lst-ib")).sendKeys("letskodeit");
		//driver.findElement(By.xpath(".//*[@id='tsf']/div[2]/div[3]/center/input[1]")).click();
		driver.findElement(By.name("btnK")).click();
		
		//Close Firefox browser		 
        //driver.quit();

	}

}
