package Chapter11WebElements;

import static org.junit.Assert.*;

import java.util.concurrent.TimeUnit;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.MarionetteDriver;

public class ElementState {
	private WebDriver driver;
	String baseURL="https://letskodeit.teachable.com/";

	@Before
	public void setUp() throws Exception {
		//Set
		System.setProperty("webdriver.gecko.driver", "E:\\Ralis\\Selenium\\geckodriver.exe");
			
		driver=new MarionetteDriver();
		
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		driver.manage().window().maximize();		
	}
	
	@Test
	public void test() {
		driver.get(baseURL);
		
		
		WebElement el01=driver.findElement(By.id("lst-ib"));
		System.out.println("State of search box: " + el01.isDisplayed());
	}

	@After
	public void tearDown() throws Exception {
	}

	
}
