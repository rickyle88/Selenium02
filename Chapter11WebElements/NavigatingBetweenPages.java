package Chapter11WebElements;

import static org.junit.Assert.*;

import java.util.concurrent.TimeUnit;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.MarionetteDriver;

public class NavigatingBetweenPages {
	
	private WebDriver driver;
	String baseURL="https://letskodeit.teachable.com/";
	
	@Before
	public void setUp() throws Exception {
		//Set
		System.setProperty("webdriver.gecko.driver", "E:\\Ralis\\Selenium\\geckodriver.exe");
			
		driver=new MarionetteDriver();
		
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		driver.manage().window().maximize();		
}
	@Test
	public void test() throws InterruptedException {
		
		driver.get(baseURL);
		String title=driver.getTitle();
		System.out.println("Current title is: " + title);
		
		String currentURL = driver.getCurrentUrl();
		
		System.out.println("Current URL is: " + currentURL);
		
		//URL of login page
		String urlToNavigate="https://sso.teachable.com/secure/42299/users/sign_in?reset_purchase_session=1";
		//Navigate to login page
		driver.navigate().to(urlToNavigate);
		
		currentURL=driver.getCurrentUrl();
		System.out.println("Current URL is: " + currentURL);
		
		Thread.sleep(2000);
		
		//Back	
		driver.navigate().back();
		System.out.println("Navigate back");
		currentURL=driver.getCurrentUrl();
		System.out.println("Current URL is: " + currentURL);
		
		Thread.sleep(2000);
		
		//Forward
		driver.navigate().forward();
		System.out.println("Navigate forward");
		currentURL=driver.getCurrentUrl();
		System.out.println("Current URL is: " + currentURL);
		
		//Refresh
		driver.navigate().refresh();
		System.out.println("Refresh");		
	}

	@After
	public void tearDown() throws Exception {
		driver.quit();
	}

	

}
