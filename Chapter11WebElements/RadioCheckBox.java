package Chapter11WebElements;

import static org.junit.Assert.*;

import java.util.concurrent.TimeUnit;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.MarionetteDriver;

public class RadioCheckBox {
	private WebDriver driver;
	String baseURL="https://letskodeit.teachable.com/";
	@Before
	public void setUp() throws Exception {
		//Set
		System.setProperty("webdriver.gecko.driver", "E:\\Ralis\\Selenium\\geckodriver.exe");
			
		driver=new MarionetteDriver();
		
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		driver.manage().window().maximize();			
	}
	
	@Test
	public void test() throws InterruptedException {
		driver.get(baseURL);
		
		driver.findElement(By.linkText("Practice")).click();
		
		//Radio buttons
		WebElement bmwRadio=driver.findElement(By.id("bmwradio"));
		bmwRadio.click();
		
		Thread.sleep(2000);
		
		WebElement benzRadio=driver.findElement(By.id("benzradio"));
		benzRadio.click();
		
		//Checkbox 
		WebElement bmwCheckbox=driver.findElement(By.id("bmwcheck"));
		bmwCheckbox.click();
		
		Thread.sleep(2000);
		
		WebElement benzCheckbox=driver.findElement(By.id("benzcheck"));
		benzCheckbox.click();
		
		
		//Verify
		System.out.println("BMW radio buttion is selected: " + bmwRadio.isSelected());
		System.out.println("BMW checkbox is selected: " + bmwCheckbox.isSelected());
		
		System.out.println("Benz radio is selected: " + benzRadio.isSelected());
		System.out.println("Benz checkbox is selected: " + benzCheckbox.isSelected());
	}
	@After
	public void tearDown() throws Exception {
		Thread.sleep(2000);
		
	}

	

}
