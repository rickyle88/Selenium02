package Chapter11WebElements;

import static org.junit.Assert.*;

import java.util.List;
import java.util.concurrent.TimeUnit;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.MarionetteDriver;
import org.openqa.selenium.support.ui.Select;

public class DropDown {
	private WebDriver driver;
	String baseURL1="https://letskodeit.teachable.com/p/practice";
	String baseURL2="https://www.expedia.com/";
	@Before
	public void setUp() throws Exception {
		//Set
		System.setProperty("webdriver.gecko.driver", "E:\\Ralis\\Selenium\\geckodriver.exe");
						
		driver=new MarionetteDriver();
		
		driver.manage().timeouts().implicitlyWait(3, TimeUnit.SECONDS);
		driver.manage().window().maximize();			
	}

	

	//https://www.expedia.com
	@Test
	public void testExpedia() throws InterruptedException {		
		
		driver.get(baseURL2);
		
		//Click flight tab
		//or xpath:=>   //span[text()='Flights']
		driver.findElement(By.xpath("//span[contains(text(),'Flights')]")).click();
		Thread.sleep(2000);
		//Flight adults dropdown
		Select selAdults=new Select(driver.findElement(By.id("flight-adults")));
		Select selChildren=new Select(driver.findElement(By.id("flight-children")));
		
		
		//Select by index
		//selChildren.selectByIndex(4);;
		
//		//Get all the value of dropdown
		List<WebElement> optionsAdults=selAdults.getOptions();
		int size=optionsAdults.size();
		
		for(int i=0;i<size;i++){
			String optionName=selAdults.getOptions().get(i).getText();
			System.out.println(optionName);
		}
		

		//Select by value
		selAdults.selectByValue("4");
		//selAdults.selectByIndex(4);
		selChildren.selectByValue("4");
		Thread.sleep(2000);
	}
	
//	@Test
//	public void testExpedia() {
//		//driver.get(baseURL2);
		//WebElement childDropdown=driver.findElement(By.id("package-1-age-select-1"));
		//=> failed because element not exits in DOM
//	}
//	
	@After
	public void tearDown() throws Exception {
	}

}
