package Chapter11WebElements;

import static org.junit.Assert.*;

import java.util.concurrent.TimeUnit;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.MarionetteDriver;

public class HiddenElements {
	private WebDriver driver;
	String baseURL1="https://letskodeit.teachable.com/p/practice";
	String baseURL2="https://www.expedia.com/";
	@Before
	public void setUp() throws Exception {
		//Set
		System.setProperty("webdriver.gecko.driver", "E:\\Ralis\\Selenium\\geckodriver.exe");
						
		driver=new MarionetteDriver();
		
		driver.manage().timeouts().implicitlyWait(3, TimeUnit.SECONDS);
		driver.manage().window().maximize();			
	}

	

	@Test
	public void testLetsKodeIt() throws InterruptedException {
		
		//Hidden element but still exists
		driver.get(baseURL1);
		WebElement textBox=driver.findElement(By.id("displayed-text"));
		System.out.println("Text box displayed: " + textBox.isDisplayed());
		
		Thread.sleep(2000);
		//Hide text box
		WebElement hideButton=driver.findElement(By.id("hide-textbox"));
		hideButton.click();
		
		System.out.println("Text box displayed: " + textBox.isDisplayed());
		
		Thread.sleep(2000);
		
		//Show textbox
		WebElement showButton=driver.findElement(By.id("show-textbox"));
		showButton.click();
		
		System.out.println("Text box displayed: " + textBox.isDisplayed());
	}
	
//	@Test
//	public void testExpedia() {
//		//driver.get(baseURL2);
		//WebElement childDropdown=driver.findElement(By.id("package-1-age-select-1"));
		//=> failed because element not exits in DOM
//	}
//	
	@After
	public void tearDown() throws Exception {
	}

}
