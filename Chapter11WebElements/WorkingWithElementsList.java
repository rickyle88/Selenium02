package Chapter11WebElements;

import static org.junit.Assert.*;


import java.util.List;
import java.util.concurrent.TimeUnit;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.MarionetteDriver;

public class WorkingWithElementsList {
	
	private WebDriver driver;
	String baseURL="https://letskodeit.teachable.com/";
	
	@Before
	public void setUp() throws Exception {
		//Set
		System.setProperty("webdriver.gecko.driver", "E:\\Ralis\\Selenium\\geckodriver.exe");
			
		driver=new MarionetteDriver();
		
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		driver.manage().window().maximize();	
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void test() throws InterruptedException {
		driver.get(baseURL);
		////input[contains(@type,'radio') and contains(@name,'cars')]
		
		//Go to practice page
		driver.findElement(By.linkText("Practice")).click();
		
		
		Thread.sleep(2000);
		boolean isChecked=false;
		
		//List of radio buttons only
		//List<WebElement> radioButtons=driver.findElements(By.xpath("//input[contains(@type,'radio') and contains(@name,'cars')]"));
		
		//List of radio buttons and check box
		List<WebElement> radioButtons=driver.findElements(By.name("cars"));
		
		//Size of the list
		System.out.println("Size of the list: " + radioButtons.size());
		
		for(int i=0;i<radioButtons.size();i++){
			//Check
			isChecked=radioButtons.get(i).isSelected();
			
			if(!isChecked){
				radioButtons.get(i).click();
				Thread.sleep(2000);
			}
			
		}
		
	}

}
