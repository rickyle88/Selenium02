
public class StringDemo {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		//String Literal
		String str1="Hello";
		String str3="Hello";
		
		String str5="  Space     ";
		
		//String Object
		String str2=new String("Welcome");
		
		//length of the string
		System.out.println("Length of str1: " + str1.length());
		
		//returns!a!char!value!at!the!given!index!number
		System.out.println("Char at: " + str1.charAt(1));
		
		//combines!specified!string!at!the!end!of!this!string
		System.out.println(str1.concat(" Appended text"));
		
		//returns!true!if!chars!are!found!in!the!string
		System.out.println(str1.contains("lo"));
		
		//checks!if!this!string!starts!with!given!prefix!
		System.out.println(str1.startsWith("He"));
		
		//checks!if!this!string!ends!with!given!suffix
		System.out.println(str1.endsWith("lo"));
		
		//compares!the!contents!of!two!given!strings!
		System.out.println(str1.equals(str2));	//Fale
		System.out.println(str1.equals(str3));	//True
		
		//returns!index!of!given!character!value!or!substring!
		System.out.println(str1.indexOf('o'));	//4
		
		//checks!if!this!string!is!empty!
		System.out.println(str1.isEmpty());
		
		//eliminates!leading!and!trailing!spaces!
		System.out.println(str5.trim());
		
		//returns!a!string!replacing!all!the!old!char!to!new!char
		System.out.println(str1.replace('o', 'i'));
		
		
		//returns!a!part!of!the!string
		System.out.println(str1.substring(2,4));
		
		//converts!this!string!into!character!array
		char[] myCharArray=str1.toCharArray();
		for(int i=0;i<myCharArray.length;i++){
			System.out.println("Index " + i + " is: " + myCharArray[i]);
		}
		
		
		//returns!the!string!in!lowercase!letter
		System.out.println("lower case: " + str1.toLowerCase());
		
		//returns!the!string!in!uppercase!letter
		System.out.println("lower case: " + str1.toUpperCase());
		
		

	}

}
