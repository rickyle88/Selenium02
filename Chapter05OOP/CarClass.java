package Chapter05OOP;

public class CarClass {
	private String make;
	private int speed;
	private int gear;
	//Constructor
	
	public CarClass(){
		
		//We can call constructor with arg inside this => this(10,2);
		this.speed=0;
		this.gear=0;
		System.out.println("Executing constructor without arguments");
	}
	
	public CarClass(int speed,int gear){
		this.speed=speed;
		this.gear=gear;
		System.out.println("Executing constructor with arguments");
	}
	
	public void setMake(String carMake){
		this.make=carMake;
	}
	
	public String returnMake(){
		return this.make;
	}
}
