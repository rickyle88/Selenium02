package Chapter05OOP;

public class Inheritance_BMW extends Inheritance_Cars{
	
	//Use keyword super to inherit from Cars
	public Inheritance_BMW (int start_speed){
		super(start_speed);
	}

	@Override
	public void increaseSpeed() {
		// TODO Auto-generated method stub
		super.increaseSpeed();
	}
	
	public void headupDisplayNav(){
		System.out.println("BMW specification ");
	}

	@Override
	public void decreaseSpeed() {
		// TODO Auto-generated method stub
		super.decreaseSpeed();
	}

}
