package Chapter05OOP;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class Exception_Account {
	
	public Connection getConn() throws SQLException{
		Connection conn=null;
		String url="jdbc:mysql://localhost:8080";
		String user="root";
		String password="";
		conn=DriverManager.getConnection(url, user, password);
		return conn;
	}
	
	public void withdraw(int amout) throws SQLException{
		getConn();
	}

}
