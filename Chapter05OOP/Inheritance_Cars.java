package Chapter05OOP;

public class Inheritance_Cars {
	
	int speed;
	
	//Constructor
	public Inheritance_Cars(int start_speed){
		this.speed=start_speed;
	}
	
	public void increaseSpeed(){
		speed++;
		System.out.println("Increasing speed");
	}
	
	public void decreaseSpeed(){
		speed--;
		System.out.println("Decreasing speed");
	}

}
