package Chapter13;

import static org.junit.Assert.*;

import java.util.concurrent.TimeUnit;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.MarionetteDriver;

public class ImplicitWaitDemo {
	
	private WebDriver driver;
	String baseURL="https://letskodeit.teachable.com/p/practice";
	
	@Before
	public void setUp() throws Exception {
		//Set
		System.setProperty("webdriver.gecko.driver", "E:\\Ralis\\Selenium\\geckodriver.exe");
			
		driver=new MarionetteDriver();		
		
		//Maximize browser's window
		driver.manage().window().maximize();	
		
		//An implicit wait is to tell WebDriver to poll the DOM for a certain amount of time when trying to find an element or elements if they are not immediately available. 
		//The default setting is 0. Once set, the implicit wait is set for the life of the WebDriver object instance.
		//All the lifecyle of driver
		driver.manage().timeouts().implicitlyWait(3, TimeUnit.SECONDS);
}
	@Test
	public void test() throws InterruptedException {
		
		driver.get(baseURL);
		
		driver.findElement(By.linkText("Login")).click();
		
		driver.findElement(By.xpath("//input[@type='email']")).sendKeys("Text");
		
	}

	@After
	public void tearDown() throws Exception {
		//driver.quit();
	}

	

}
