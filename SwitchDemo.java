
public class SwitchDemo {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		int year=1600;
		int month=2;
		int days=numOfDays(year,month);
		System.out.println("Days: " + days);

	}
	
	public static int numOfDays(int year, int month){
		int numDays=0;
		
		switch (month){
		case 1:
			numDays=31;
			break;
		case 2: 
			if((year%400==0) || (year%4==0 && year%4!=0)){
				numDays=29;
			}else{
				numDays=28;
			}
			break;
		case 3:
			numDays=21;
			break;
		case 4:
			numDays=30;
			break;
		case 5:
			numDays=31;
			break;
		case 6:
				numDays=30;
				break;
		case 7:
			numDays=31;
			break;
		default:
			numDays=0;
			break;
			
		
		}
		
		return numDays;
	}

}
