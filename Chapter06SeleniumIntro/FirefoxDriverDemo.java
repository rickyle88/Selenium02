package Chapter06SeleniumIntro;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.MarionetteDriver;

public class FirefoxDriverDemo {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		//Set
		System.setProperty("webdriver.gecko.driver", "E:\\Ralis\\Selenium\\geckodriver.exe");
		
		WebDriver driver=new MarionetteDriver();
		String baseURL="http://www.google.com";
		driver.get(baseURL);
		System.out.println("Application title is ============="+driver.getTitle());
		
		//Close Firefox browser		 
        driver.quit();


	}

}
