package Chapter06SeleniumIntro;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.safari.SafariDriver;

public class SafariDriverDemo {

	public static void main(String[] args) throws InterruptedException {
		// TODO Auto-generated method stub
		
		//https://github.com/SeleniumHQ/selenium/wiki/SafariDriver
		//Index of /2.48/ http://selenium-release.storage.googleapis.com/2.48/SafariDriver.safariextz
		//Use Safari to open it
		
		WebDriver driver=new SafariDriver();
		driver.get("http://www.google.com");
		Thread.sleep(3000);
		driver.quit();

	}

}
