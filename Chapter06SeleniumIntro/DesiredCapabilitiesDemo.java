package Chapter06SeleniumIntro;

import org.openqa.selenium.Platform;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.remote.DesiredCapabilities;

public class DesiredCapabilitiesDemo {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		//Set
		System.setProperty("webdriver.gecko.driver", "E:\\Ralis\\Selenium\\geckodriver.exe");
		
		//URL
		String baseURL="http://www.google.com";
				
		WebDriver driver;		
		
		DesiredCapabilities caps=DesiredCapabilities.firefox();
		caps.setBrowserName("firefox");
		
		caps.setPlatform(Platform.WIN10);
		
		driver=new FirefoxDriver(caps);
		
		driver.get(baseURL);
		driver.manage().window().maximize();
		

	}

}
