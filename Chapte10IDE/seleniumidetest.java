package Chapte10IDE;

import static org.junit.Assert.fail;

import java.util.concurrent.TimeUnit;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
//import org.openqa.selenium.firefox.MarionetteDriver;
//import org.openqa.selenium.firefox.MarionetteDriver;
import org.openqa.selenium.firefox.MarionetteDriver;

public class seleniumidetest {
	//private WebDriver driver;
	private String baseUrl;
	private WebDriver driver;
	private StringBuffer verificationErrors = new StringBuffer();
	
	

	@Before
	public void setUp() throws Exception {
		//Set
		System.setProperty("webdriver.gecko.driver", "E:\\Ralis\\Selenium\\geckodriver.exe");
		
		driver=new MarionetteDriver();
		baseUrl = "https://letskodeit.teachable.com/";
		
		//30 seconds
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		driver.manage().window().maximize();
	}

	@Test
	public void testUntitled() throws Exception {
		driver.get(baseUrl + "/");
		driver.findElement(By.linkText("Practice")).click();
		System.out.println(driver.findElement(By.tagName("h1")).getText());
		System.out.println("Application title is ============="+driver.getTitle());
	}

	@After
	public void tearDown() throws Exception {
		//3 seconds
		Thread.sleep(3000);
		
		driver.quit();
		String verificationErrorString = verificationErrors.toString();
		if (!"".equals(verificationErrorString)) {
			fail(verificationErrorString);
		}
	}

}
