import java.util.Scanner;

public class ReverseChars {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner scan=new Scanner(System.in);
		
		System.out.println("Input string: ");
		String inputStr=scan.nextLine();
		
		while(inputStr==null || inputStr.isEmpty()){
			System.out.println("Please input valid string");
			inputStr=scan.nextLine();
		}
		
		scan.close();
		
		System.out.println(reverseChars(inputStr));
		
	}
	
	public static String reverseChars(String inputStr){
		
		String reverseChars="";
		
		for(int i=inputStr.length()-1;i>=0;i--){
			reverseChars=reverseChars+inputStr.charAt(i);
		}
		
		return reverseChars;
	}

}
