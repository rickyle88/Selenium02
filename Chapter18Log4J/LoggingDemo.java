package Chapter18Log4J;

import java.util.Properties;

import org.apache.log4j.BasicConfigurator;
import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;

public class LoggingDemo {

	
	//Logger log=Logger.getLogger("My Logger");
	static Logger log=Logger.getLogger(LoggingDemo.class);
			
			
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		//Configuration
		//BasicConfigurator.configure();
		
		//Create properties file for log4j	=	CA: name
		Properties log4jProp=new Properties();
		log4jProp.setProperty("log4j.rootLogger", "DEBUG, CA");
		log4jProp.setProperty("log4j.appender.CA", "org.apache.log4j.ConsoleAppender");
		log4jProp.setProperty("log4j.appender.CA.layout", "org.apache.log4j.PatternLayout");
		log4jProp.setProperty("log4j.appender.CA.layout.ConversionPattern", "%d{dd-mm-yyyy} -- %5p %c - %m%n");
		PropertyConfigurator.configure(log4jProp);
		
		
		
		
		//////////
		log.info("This is a info");		 
		runMethod();
		log.info("This message is a debug");

	}
	
	public static void runMethod(){
		log.info("This is from run Method");
	}

}
