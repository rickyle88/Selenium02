package Chapter12MethodsProperties;

import static org.junit.Assert.*;

import java.util.List;
import java.util.concurrent.TimeUnit;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.MarionetteDriver;

import Chapter12MethodsProperties.getElementList;;

public class getElementListDemo {
	private WebDriver driver;
	String baseURL1="https://letskodeit.teachable.com/p/practice";
	private getElementList gm;
	@Before
	public void setUp() throws Exception {
		//Set
		System.setProperty("webdriver.gecko.driver", "E:\\Ralis\\Selenium\\geckodriver.exe");
		
		
		driver=new MarionetteDriver();
		gm=new getElementList(driver);
		
		driver.manage().timeouts().implicitlyWait(3, TimeUnit.SECONDS);
		driver.manage().window().maximize();			
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void test() {
		driver.get(baseURL1);
		
		
		
		List<WebElement> buttonOpenTab=gm.getElement("//input[@type='text']", "xpath");
		//WebElement buttonOpenTab=driver.findElement(By.id("opentab"));
		
		int size=buttonOpenTab.size();
		
		System.out.println("Size of the elements list: " + size);
		
		//buttonOpenTab.get(0).sendKeys("Hello world");
		for(WebElement e : buttonOpenTab){
			e.sendKeys("Hello");
		}
		
		//////////////////////////////////////
		boolean result=gm.isElementPresent("name1", "id");
		System.out.println("Size of the element list is: " + result);
	
	}

}
