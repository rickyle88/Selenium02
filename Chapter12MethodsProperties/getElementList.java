package Chapter12MethodsProperties;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class getElementList {
	
	WebDriver driver;
	
	public getElementList(WebDriver driver){
		this.driver=driver;
	}
	
	public List<WebElement> getElement(String locator, String type){
		type=type.toLowerCase();
		if(type.equals("id")){
			System.out.println("Element found with id: " + type);
			return this.driver.findElements(By.id(locator));
		}else if(type.equals("xpath")){
			System.out.println("Element found with xpath: " + type);
			return this.driver.findElements(By.xpath(locator));
		}else{
			System.out.println("Locator type not supported");
			return null;
		}
	}
	
	//Check if elements are present
	public boolean isElementPresent(String locator,String type){
		List<WebElement> elementList=getElement(locator,type);
		
		if(elementList.size()>0){
			return true;
		}else{
			return false;
		}
	}

}
