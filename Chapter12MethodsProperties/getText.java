package Chapter12MethodsProperties;

import static org.junit.Assert.*;

import java.util.concurrent.TimeUnit;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.MarionetteDriver;

public class getText {
	private WebDriver driver;
	String baseURL1="https://letskodeit.teachable.com/p/practice";
	@Before
	public void setUp() throws Exception {
		//Set
		System.setProperty("webdriver.gecko.driver", "E:\\Ralis\\Selenium\\geckodriver.exe");
						
		driver=new MarionetteDriver();
		
		driver.manage().timeouts().implicitlyWait(3, TimeUnit.SECONDS);
		driver.manage().window().maximize();			
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void test() {
		driver.get(baseURL1);
		
		WebElement buttonOpenTab=driver.findElement(By.id("opentab"));
		
		//Get text
		String buttonOpenTab_text=buttonOpenTab.getText();
		System.out.println(buttonOpenTab_text);
		
		WebElement textBoxName=driver.findElement(By.id("name"));
		String attr01= textBoxName.getAttribute("class");
		String attr02=textBoxName.getAttribute("type");
		System.out.println(attr01 + " " + attr02);
	}

}
