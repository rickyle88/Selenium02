
public class ArrayDemo {
	public static void main(String[] args) {
		
		//Declare new intarray
		
		int[] myIntArray={100,90,80};
		
		/*
		myIntArray=new int[10];
		
		myIntArray[0]=100;
		myIntArray[1]=90;
		*/
		System.out.println("0 index: " + myIntArray[0]);
		System.out.println("1 index: " + myIntArray[1]);
		System.out.println("2 index: " + myIntArray[2]);
		
		
		String[] myStringArray={"toyota","honda","audi"};
		System.out.println("0 index: " + myStringArray[0]);
		System.out.println("1 index: " + myStringArray[1]);
		System.out.println("2 index: " + myStringArray[2]);
		
		System.out.println(myIntArray.length);
		System.out.println(myStringArray.length);
		
		for(int i=0;i<myIntArray.length;i++){
			System.out.println(myIntArray[i]);
		}
	}
}
