
public class WhileLoop {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		int i=10;
		boolean condition1=false;
		
		System.out.println(condition1);
		
		while(i<20){
			System.out.println(i);
			i++;
			if(i==15){
				break;
			}
		}
		
		int j=10;
		do{
			System.out.println(j);
			j++;
		}while(j<20);

	}

}
