package Chapter17AutomationFramework;

import static org.junit.Assert.*;

import java.util.concurrent.TimeUnit;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

public class PageObjectModel {

	private WebDriver driver;
	String baseURL="https://www.expedia.com/";
	//SearchPage sp=new SearchPage();
	@Before
	public void setUp() throws Exception {
		driver=new FirefoxDriver();
		
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(3, TimeUnit.SECONDS);
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void test() {
		driver.get(baseURL);
		SearchPage.navigateToFlightsTab(driver);
		SearchPage.fillOriginTextBox(driver, "New York");
		SearchPage.destinationTextBox(driver).sendKeys("Chicago");
		SearchPage.departureDateTextBox(driver).sendKeys("12/25/2017");
		SearchPage.returnDateTextBox(driver).sendKeys("12/31/2017");
		SearchPage.clickSearchButton(driver);
		
		
	
	}

}
