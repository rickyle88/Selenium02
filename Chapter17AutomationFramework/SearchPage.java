package Chapter17AutomationFramework;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class SearchPage {
	
	public static WebElement element=null;
	
	///////////////////
	//Returns the flight origin text box element (Flying from)
	public static WebElement originTextBox(WebDriver driver){
		element=driver.findElement(By.id("flight-origin"));
		return element;
	}
	
	//Send keys to origin text box
	public static void fillOriginTextBox(WebDriver driver, String origin){
		element=originTextBox(driver);
		element.sendKeys(origin);
	}
		
	/////////////////////
	//Returns the flight destination text box element (Flying to)
	public static WebElement destinationTextBox(WebDriver driver){
		element=driver.findElement(By.id("flight-destination"));
		return element;
	}
	
	//Returns the departure date text box element (Departing)
	public static WebElement departureDateTextBox(WebDriver driver){
		element=driver.findElement(By.id("flight-departing"));
		return element;
	}
	
	
	//Returns the return date text box element (Returning)
	public static WebElement returnDateTextBox(WebDriver driver){
		element=driver.findElement(By.id("flight-returning"));
		return element;
	}
	
	
	//Returns the search button box element
	public static WebElement searchButton(WebDriver driver){
		element=driver.findElement(By.id("search-button"));
		return element;
	}
	
	//Click on search button
	public static void clickSearchButton(WebDriver driver){
		element=searchButton(driver);
		element.click();
	}
	
	//Naviagte to Flights tab ()
	public static void navigateToFlightsTab(WebDriver driver){
		element=driver.findElement(By.id("tab-flight-tab"));
		element.click();
	}
	
}
