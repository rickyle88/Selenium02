package Chapter17AutomationFramework;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class SearchPageFactory {
	
	WebDriver driver;
	
	@FindBy(id="header-history")
	WebElement headerHistory;
	
	@FindBy(id="tab-flight-tab")
	WebElement flightsTab;
	
	@FindBy(id="flight-type-roundtrip-label")
	WebElement roundTrip;
	
	@FindBy(id="flight-type-multi-dest-label")
	WebElement multipleDestination;
	
	//Flying from
	@FindBy(id="flight-origin")
	WebElement originCity;
	
	//Flying to
	@FindBy(id="flight-destination")
	WebElement destinationCity;
	
	//Departure date
	@FindBy(id="flight-departing")
	WebElement departureDate;
		
	//Return date
	@FindBy(id="flight-returning")
	WebElement returnDate;
	
	public SearchPageFactory(WebDriver driver) {
		this.driver=driver;
		PageFactory.initElements(driver, this);
	}

	//Constructor
	
	
	
	public void clickFlightTab(){
		flightsTab.click();
	}
	
	
	public void clickRoundTrip(){
		roundTrip.click();
	}
	
	public void clickMultipleDestination(){
		multipleDestination.click();
	}
	
	//Flying from
	public void setOriginCity(String origin){
		originCity.sendKeys(origin);
	}
	
	//Flying to
	public void setDestinationCity(String destination){
		destinationCity.sendKeys(destination);
	}
	
	
	
	

}
