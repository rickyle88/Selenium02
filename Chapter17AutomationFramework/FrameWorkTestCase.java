package Chapter17AutomationFramework;

import static org.junit.Assert.*;

import java.util.concurrent.TimeUnit;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

public class FrameWorkTestCase {
	
	private WebDriver driver;
	String baseURL="https://www.expedia.com/";
	SearchPageFactory searchPage;

	@Before
	public void setUp() throws Exception {
		driver=new FirefoxDriver();
		searchPage=new SearchPageFactory(driver);
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(3, TimeUnit.SECONDS);
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void test() {
		driver.get(baseURL);
		searchPage.clickFlightTab();
		searchPage.setOriginCity("New York");
		searchPage.setDestinationCity("Chicago");
	}

}
