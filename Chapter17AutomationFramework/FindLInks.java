package Chapter17AutomationFramework;

import static org.junit.Assert.*;

import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;

public class FindLInks {
	
	private WebDriver driver;
	String baseURL="https://www.expedia.com/";
	SearchPageFactory searchPage;

	@Before
	public void setUp() throws Exception {
		driver=new FirefoxDriver();
		searchPage=new SearchPageFactory(driver);
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(3, TimeUnit.SECONDS);
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void test() {
		driver.get(baseURL);
		
		searchPage.clickFlightTab();

		List<WebElement> linkLIst=clickableLinks(driver);
		for(WebElement link : linkLIst){
			String href=link.getAttribute("href");
			try{
				System.out.println("URL: " + href + " returned" + linkStatus(new URL(href)));				
			}
			catch(Exception e){				
			}
		}
		
	}
	
	//Create a list of webelement with clickable links
	public static List<WebElement> clickableLinks(WebDriver driver){
		
		List<WebElement> linksToClick=new ArrayList<WebElement>();
		
		//Create list of elements with a tag
		List<WebElement> elements=driver.findElements(By.tagName("a"));
		
		//Add elements with img tag to a tag
		elements.addAll(driver.findElements(By.tagName("img")));
		
		for(WebElement e : elements){
			if(e.getAttribute("href")!=null){
				linksToClick.add(e);
			}
		}
		
		return linksToClick;
	}
	
	//Check link status
	public static String linkStatus(URL url){
		try{
			HttpURLConnection http=(HttpURLConnection) url.openConnection();
			http.connect();
			String respondMessage=http.getResponseMessage();
			http.disconnect();
			return respondMessage;
		}
		catch(Exception e){
			return e.getMessage();
		}
		
	}

}
