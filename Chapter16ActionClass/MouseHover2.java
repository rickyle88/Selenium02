package Chapter16ActionClass;

import static org.junit.Assert.*;


import java.util.concurrent.TimeUnit;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;

public class MouseHover2 {
	
	private WebDriver driver;
	String baseURL="http://www.amazon.com";
	
	private JavascriptExecutor js;
	@Before
	public void setUp() throws Exception {
		// TODO Auto-generated method stub
		System.setProperty("webdriver.chrome.driver", "E:\\Ralis\\Selenium\\chromedriver.exe");
		 
		// Initialize browser
		driver=new ChromeDriver();		
		
		js = (JavascriptExecutor) driver;
		
		//Maximize browser's window
		driver.manage().window().maximize();	
		
		driver.manage().timeouts().implicitlyWait(4, TimeUnit.SECONDS);
}
	@Test
	public void test() throws InterruptedException {
		
		driver.get(baseURL);
		
		
		//Scroll down to Mouse hover button
		//js.executeScript("window.scrollBy(0,700);");
		
		//Thread.sleep(2000);
		
		//Mouse hover button
		//WebElement mainElement=driver.findElement(By.id("mousehover"));
		WebElement mainElement=driver.findElement(By.xpath("//span[text()='Departments']"));
		
		//Hover
		Actions actionHover=new Actions(driver);
		actionHover.moveToElement(mainElement).perform();
		//Thread.sleep(2000);
		//Fire Tablets
		
		//Mouse hover button => Top
		//WebElement subElement=driver.findElement(By.linkText("Top"));
		WebElement subElement=driver.findElement(By.xpath("//span[text()='Fire Tablets']"));
		actionHover.moveToElement(subElement).perform();
		//subElement.click();
		
		//Thread.sleep(2000);
		
		WebElement subElement1=driver.findElement(By.xpath("//span[text()='Fire HD 6']"));
		//actionHover.moveToElement(subElement1).click().perform();;
		subElement1.click();
		
		
		
	}

	@After
	public void tearDown() throws Exception {
		Thread.sleep(2000);
		//driver.quit();
	}

	

}
