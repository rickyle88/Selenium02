package Chapter16ActionClass;

import static org.junit.Assert.*;

import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.MarionetteDriver;
import org.openqa.selenium.interactions.Actions;

public class DragDrop {
	
	private WebDriver driver;
	String baseURL="https://jqueryui.com/droppable/";
	
	@Before
	public void setUp() throws Exception {
		// TODO Auto-generated method stub
		System.setProperty("webdriver.chrome.driver", "E:\\Ralis\\Selenium\\chromedriver.exe");
		 
		// Initialize browser
		driver=new ChromeDriver();			
		
		//Maximize browser's window
		driver.manage().window().maximize();	
		
		driver.manage().timeouts().implicitlyWait(3, TimeUnit.SECONDS);
}
	@Test
	public void test() throws InterruptedException {
		
		driver.get(baseURL);
		
		driver.switchTo().frame(0);
		
		WebElement fromElement=driver.findElement(By.id("draggable"));
		WebElement toElement=driver.findElement(By.id("droppable"));
		
		Actions myaction=new Actions(driver);
		
		//Drag and drop
		myaction.dragAndDrop(fromElement, toElement).build().perform();
		//Click and hold, move to element, release, build and perform
		myaction.clickAndHold(fromElement).moveToElement(toElement).release().build();
		
		
		
	}

	@After
	public void tearDown() throws Exception {
		//driver.quit();
	}

	

}
