package Chapter16ActionClass;

import static org.junit.Assert.*;


import java.util.concurrent.TimeUnit;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;

public class MouseHover3 {
	
	private WebDriver driver;
	String baseURL="https://letskodeit.teachable.com/p/practice";
	String baseURL2="http://store.demoqa.com/";
	private JavascriptExecutor js;
	@Before
	public void setUp() throws Exception {
		// TODO Auto-generated method stub
		System.setProperty("webdriver.chrome.driver", "E:\\Ralis\\Selenium\\chromedriver.exe");
		 
		// Initialize browser
		driver=new ChromeDriver();		
		
		js = (JavascriptExecutor) driver;
		
		//Maximize browser's window
		driver.manage().window().maximize();	
		
		driver.manage().timeouts().implicitlyWait(4, TimeUnit.SECONDS);
}
	@Test
	public void test() throws InterruptedException {
		
		driver.get(baseURL);
		
		
		//Scroll down to Mouse hover button
		js.executeScript("window.scrollBy(0,700);");
		
		
		Thread.sleep(3000);
		//Mouse hover button
		WebElement mainElement=driver.findElement(By.id("mousehover"));
		
		
		//Hover
		Actions actionHover=new Actions(driver);
		actionHover.moveToElement(mainElement).perform();
		
		//Thread.sleep(3000);
		
		//Mouse hover button => Top
		WebElement subElement=driver.findElement(By.linkText("Top"));
		
		actionHover.moveToElement(subElement).click().perform();
		//subElement.click();
		
		
		
	}

	@After
	public void tearDown() throws Exception {
		Thread.sleep(2000);
		//driver.quit();
	}

	

}
