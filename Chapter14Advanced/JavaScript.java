package Chapter14Advanced;

import static org.junit.Assert.*;

import java.util.concurrent.TimeUnit;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.MarionetteDriver;

public class JavaScript {
	
	private WebDriver driver;
	private JavascriptExecutor js;
	String baseURL="https://letskodeit.teachable.com/p/practice";
	
	@Before
	public void setUp() throws Exception {
		//Set
		System.setProperty("webdriver.gecko.driver", "E:\\Ralis\\Selenium\\geckodriver.exe");
			
		driver=new MarionetteDriver();		
		js=(JavascriptExecutor) driver;
		//Maximize browser's window
		driver.manage().window().maximize();	
		
		driver.manage().timeouts().implicitlyWait(3, TimeUnit.SECONDS);
}
	@Test
	public void test() throws InterruptedException {
		
		js.executeScript("window.location='https://letskodeit.teachable.com/p/practice';");
		
		WebElement textBox=(WebElement) js.executeScript("return document.getElementById('name');");
		textBox.sendKeys("Hello");
		
	}

	@After
	public void tearDown() throws Exception {
		//driver.quit();
	}

	

}
