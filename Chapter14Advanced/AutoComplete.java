package Chapter14Advanced;

import static org.junit.Assert.*;

import java.util.List;
import java.util.concurrent.TimeUnit;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.MarionetteDriver;

public class AutoComplete {
	
	private WebDriver driver;
	String baseURL="https://www.southwest.com/";
	
	@Before
	public void setUp() throws Exception {
		//Set
		System.setProperty("webdriver.gecko.driver", "E:\\Ralis\\Selenium\\geckodriver.exe");
			
		driver=new MarionetteDriver();		
		
		//Maximize browser's window
		driver.manage().window().maximize();	
		
		driver.manage().timeouts().implicitlyWait(3, TimeUnit.SECONDS);
}
	@Test
	public void test() throws InterruptedException {
		
		driver.get(baseURL);

		//
		String searchText="Newark";
		String partialText="New York";
		
		//departField
		WebElement departField=driver.findElement(By.id("air-city-departure"));
		departField.sendKeys(partialText);
		
		//List
		WebElement element=driver.findElement(By.id("air-city-departure-menu"));
		List<WebElement> results=element.findElements(By.tagName("li"));
		
		int size=results.size();
		
		for(int i=0;i<size;i++){
			System.out.println("List of result: " + results.get(i).getText());
		}
		
		for(WebElement result : results){
			if(result.getText().contains(searchText)){
				result.click();
			}
		}
				
				
		
		
	}

	@After
	public void tearDown() throws Exception {
		//driver.quit();
	}

	

}
