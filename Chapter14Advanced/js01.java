package Chapter14Advanced;

import java.util.concurrent.TimeUnit;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.MarionetteDriver;


// Please change the extension of the file to .java before using it
// I had to change the file extension to .txt because udemy does not support .java file uploading
public class js01 {
	private WebDriver driver;
	String baseUrl;
	private JavascriptExecutor js;

	@Before
	public void setUp() throws Exception {
		//Set
		System.setProperty("webdriver.gecko.driver", "E:\\Ralis\\Selenium\\geckodriver.exe");
		driver = new MarionetteDriver();
		baseUrl = "https://letskodeit.teachable.com/p/practice";
		js = (JavascriptExecutor) driver;
		
		// Maximize the browser's window
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
	}
	
	@Test
	public void testJavaScriptExecution() throws Exception {
		// Navigation
		// driver.get(baseUrl);
		js.executeScript("window.location = 'https://letskodeit.teachable.com/pages/practice';");
		
		//Size of window
		long height=(Long) js.executeScript("return window.innerHeight;");
		long width=(Long) js.executeScript("return window.innerWidth;");
		
		System.out.println(height+" "+width);
		
		//Scroll down
		js.executeScript("window.scrollBy(0,1000);");
		Thread.sleep(2000);
		//Scroll up
		js.executeScript("window.scrollBy(0,-1000);");
		Thread.sleep(2000);
		//Scroll Element into View
		WebElement mousehover=driver.findElement(By.id("mousehover"));
		js.executeScript("arguments[0].scrollIntoView(true);",mousehover);
		Thread.sleep(2000);
		
		js.executeScript("window.scrollBy(0,-100);");
	}
	
	@After
	public void tearDown() throws Exception {
	}
}