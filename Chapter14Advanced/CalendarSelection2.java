package Chapter14Advanced;

import static org.junit.Assert.*;

import java.util.List;
import java.util.concurrent.TimeUnit;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.MarionetteDriver;

public class CalendarSelection2 {
	
	private WebDriver driver;
	String baseURL="https://www.expedia.com/";
	
	@Before
	public void setUp() throws Exception {
		//Set
		System.setProperty("webdriver.gecko.driver", "E:\\Ralis\\Selenium\\geckodriver.exe");
			
		driver=new MarionetteDriver();		
		
		//Maximize browser's window
		driver.manage().window().maximize();	
		
		driver.manage().timeouts().implicitlyWait(3, TimeUnit.SECONDS);
}
	@Test
	public void test() throws InterruptedException {
		
		driver.get(baseURL);
		
		//Click flight tab
		driver.findElement(By.id("tab-flight-tab")).click();
		
		//Find Departing field
		WebElement departingField=driver.findElement(By.id("flight-departing"));
		departingField.click();
		
		//Find the month to be select
		WebElement monthToSelect=driver.findElement(By.xpath("//div[@class='datepicker-cal-month'][position()=2]"));
		
		List<WebElement> allValidDates=monthToSelect.findElements(By.tagName("button"));
		
		for(WebElement e : allValidDates){
			if(e.getText().equals("30")){
				e.click();
				break;
			}
		}
		
		
	}

	@After
	public void tearDown() throws Exception {
		//driver.quit();
	}

	

}
